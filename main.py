class Song(object):
    def __init__(self, name: str, author: str, year: int, text: str):
        self.__validate_data(name, author, year)

        self.name = name
        self.author = author
        self.year = year
        self.text = text

    def add_text(self):
        self.text = input("Enter the new text of song:\n")

    def print_text(self):
        print(self.text)

    def remove_text(self):
        if len(self.text) <= 0:
            return
        self.text = ""

    def __str__(self):
        return f"Name - {self.name}\n" \
               f"Author - {self.author}\n" \
               f"Year - {self.year}"

    def __validate_data(self, name: str, author: str, year: int):
        if len(name) <= 0:
            raise ValueError("Name can't be empty")
        elif len(author) <= 0:
            raise ValueError("Author can't be empty")
        elif year < 0:
            raise ValueError("Year can't be negative")


class MusicList(object):
    def __init__(self):
        self.songs_list = []

    def add_song(self, song: Song):
        self.songs_list.append(song)

    def print_list(self):
        number = 1
        for song in self.songs_list:
            print(f"{number} - {song}")
            number += 1

    def remove_song(self):
        if len(self.songs_list) <= 0:
            return

        self.print_list()

        choice = int(input("Enter the number of song you want to remove to:\n"))

        self.songs_list.pop(choice - 1)

    def search_by_author(self):
        author = input("Enter the author whom songs you are looking for:\n").lower()

        for song in self.songs_list:
            if author in song.author.lower():
                print(song)

    def search_by_word(self):
        word = input("Enter the word songs you are looking for with:\n").lower()

        for song in self.songs_list:
            if word in song.text.lower():
                print(song)


song = Song("Music", "Some Author", 2004, "")
song_2 = Song("Music 2", "Author 2", 2005, "")

song.add_text()

music_list = MusicList()

music_list.add_song(song)
music_list.add_song(song_2)

# music_list.search_by_author()
music_list.search_by_word()